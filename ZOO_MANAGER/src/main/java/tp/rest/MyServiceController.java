package tp.rest;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;
import tp.model.*;

import javax.xml.bind.JAXBException;
import javax.xml.ws.http.HTTPException;


import static org.springframework.http.MediaType.*;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@RestController
@EnableAutoConfiguration
public class MyServiceController {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MyServiceController.class, args);
    }

    private Center center;

    public MyServiceController() {
        // Creates a new center
        center = new Center(new LinkedList<>(), new Position(49.30494d, 1.2170602d), "Biotropica");
        // And fill it with some animals
        Cage usa = new Cage(
                "usa",
                new Position(49.305d, 1.2157357d),
                25,
                new LinkedList<>(Arrays.asList(
                        new Animal("Tic", "usa", "Chipmunk", UUID.randomUUID()),
                        new Animal("Tac", "usa", "Chipmunk", UUID.randomUUID())
                ))
        );

        Cage amazon = new Cage(
                "amazon",
                new Position(49.305142d, 1.2154067d),
                15,
                new LinkedList<>(Arrays.asList(
                        new Animal("Canine", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Incisive", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("Molaire", "amazon", "Piranha", UUID.randomUUID()),
                        new Animal("De lait", "amazon", "Piranha", UUID.randomUUID())
                ))
        );

        center.getCages().addAll(Arrays.asList(usa, amazon));
    }

    // -----------------------------------------------------------------------------------------------------------------
    // /animals
    @RequestMapping(path = "/animals", method = GET, produces = APPLICATION_JSON_VALUE)
    public Center getAnimals() {
        return center;
    }

    @RequestMapping(path = "/animals", method = POST, produces = APPLICATION_JSON_VALUE)
    public Center addAnimal(@RequestBody Animal animal) throws CageNotFoundException {
        boolean success = this.center.getCages()
                .stream()
                .filter(cage -> cage.getName().equals(animal.getCage()))
                .findFirst()
                .orElseThrow(CageNotFoundException::new)
                .getResidents()
                .add(animal);
        if (success) return this.center;
        throw new IllegalStateException("Failing to add the animal while the input was valid and it's cage was existing is not suppose to happen.");
    }

    // -----------------------------------------------------------------------------------------------------------------
    // /animals/{id}
    @RequestMapping(path = "/animals/{id}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Animal getAnimalById(@PathVariable UUID id) throws AnimalNotFoundException {
        return center.findAnimalById(id);
    }
    // /findbyname/{name}
    @RequestMapping(path = "/findbyname/{name}", method = GET, produces = APPLICATION_JSON_VALUE)
    public Animal getAnimalByName(@PathVariable String name) throws AnimalNotFoundException {
        return center.findAnimalByName(name);
    }


    // /animals/delete/{id}
    @RequestMapping(path = "/animals/delete/{id}", method = DELETE, produces = APPLICATION_JSON_VALUE)
    public Center deleteAnimalById(@PathVariable UUID id) throws AnimalNotFoundException {
        try {
            Animal animal = this.center.findAnimalById(id);
            this.center.getCages()
                    .stream()
                    .filter(cage -> cage.getName().equals(animal.getCage()))
                    .findFirst()
                    .orElseThrow(() -> new HTTPException(404))
                    .getResidents()
                    .remove(animal);
            return this.center;
        } catch (AnimalNotFoundException e) {
            throw new HTTPException(404);
        }

    }

    // /animals/update/{id}
    @RequestMapping(path = "/animals/update/{id}", method = PUT, produces = APPLICATION_JSON_VALUE)
    public Center UpdateAnimalById(@RequestBody Animal animal, @PathVariable UUID id) throws AnimalNotFoundException {

        try {
            Animal refAnimal = this.center.findAnimalById(id);
            refAnimal.setName(animal.getName());
            refAnimal.setSpecies(animal.getSpecies());
            return this.center;
        } catch (AnimalNotFoundException e) {
            throw new HTTPException(404);
        }

    }
    // /centerposition
    @RequestMapping(path = "/centerposition", method = GET, produces = APPLICATION_JSON_VALUE)
    public String getCentrePosition() throws JAXBException {

        try {
            String string = IOUtils.toString(new InputStreamReader(new URL("http://api.geonames.org/findNearbyJSON?lat=" + this.center.getPosition().getLatitude() + "&lng=" + this.center.getPosition().getLongitude() + "&username=essamaoual").openStream()));
            return string;
        } catch (MalformedURLException e) {
            throw new HTTPException(404);
        } catch (IOException e) {
            throw new HTTPException(404);
        }

    }
    // /cageposition/{name}
    @RequestMapping(path = "/cageposition/{name}", method = GET, produces = APPLICATION_JSON_VALUE)
    public String getCagePosition(@PathVariable String name) throws JAXBException, AnimalNotFoundException, CageNotFoundException {
        Animal animal = center.findAnimalByName(name);
        Cage cage = center.findCageByName(animal.getCage());
        try {
            String string = IOUtils.toString(new InputStreamReader(new URL("http://api.geonames.org/findNearbyJSON?lat=" + cage.getPosition().getLatitude() + "&lng=" + cage.getPosition().getLongitude() + "&username=essamaoual").openStream()));
            return string;
        } catch (MalformedURLException e) {
            throw new HTTPException(404);
        } catch (IOException e) {
            throw new HTTPException(404);
        }

    }


}
